import { Orientation } from '~/orientations/orientation.entity';

export const defaultOrientations: Partial<Orientation>[] = [
  { name: 'Vertical' },
  { name: 'Horizontal' },
];
