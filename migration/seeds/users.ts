import { User } from '~/users/user.entity';

export const defaultUsers: Partial<User>[] = [
  {
    auth0Id: '',
    firstName: 'Admin',
    lastName: 'Admin',
    email: 'admin_cms@email.com',
  },
];
