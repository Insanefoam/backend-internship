import { Orientation } from '~/orientations/orientation.entity';
import { MigrationInterface, QueryRunner } from 'typeorm';
import { defaultOrientations } from './seeds';

export class OrientationsSeed1619070247681 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const manager = queryRunner.connection;

    const orientationsRepository = manager.getRepository(Orientation);
    await orientationsRepository.save(defaultOrientations);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const manager = queryRunner.connection;

    const orientationsRepository = manager.getRepository(Orientation);
    await orientationsRepository.delete(
      defaultOrientations.map((orientation) => orientation.id),
    );
  }
}
