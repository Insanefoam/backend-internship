import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Inject, Injectable } from '@nestjs/common';
import { passportJwtSecret } from 'jwks-rsa';
import { JwtPayload } from '~/common/interfaces/jwt-payload';
import { auth0Config } from '~/config';
import { ConfigType } from '@nestjs/config';
import { UsersRepository } from '~/users/users.repository';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly usersRepo: UsersRepository,
    @Inject(auth0Config.KEY) config: ConfigType<typeof auth0Config>,
  ) {
    super({
      secretOrKeyProvider: passportJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: `${config.issuer}/.well-known/jwks.json`,
      }),

      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      audience: config.audience,
      issuer: `${config.issuer}/`,
      algorithms: ['RS256'],
    });
  }

  async validate(payload: JwtPayload) {
    const user = this.usersRepo.findOne({ auth0Id: payload.sub });

    if (user) {
      return user;
    }

    return null;
  }
}
