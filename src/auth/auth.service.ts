import { HttpService, Inject, Injectable } from '@nestjs/common';
import { User } from '~/users/user.entity';
import { Auth0UserInfo } from '~/common/interfaces/auth0-user-info';
import { auth0Config } from '~/config';
import { ConfigType } from '@nestjs/config';
import { UsersRepository } from '~/users/users.repository';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersRepo: UsersRepository,
    private readonly httpService: HttpService,
    @Inject(auth0Config.KEY) private config: ConfigType<typeof auth0Config>,
  ) {}

  async loginUser(token: string): Promise<User> {
    const userInfo = await this.getUserInfo(token);
    return this.usersRepo.create(userInfo);
  }

  async getUserInfo(token: string): Promise<Partial<User>> {
    const { data } = await this.httpService
      .get<Auth0UserInfo>(`${this.config.issuer}/userinfo`, {
        headers: { authorization: `Bearer ${token}` },
      })
      .toPromise();

    const partialUser: Partial<User> = {
      auth0Id: data.sub,
      email: data.email,
      firstName: data.nickname,
      lastName: '',
    };

    return partialUser;
  }
}
