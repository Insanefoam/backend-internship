export enum OrientationType {
  Horizontal = 'horizontal',
  Vertical = 'vertical',
}
export const orientationTypes = ['horizontal', 'vertical'];
