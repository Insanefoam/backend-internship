import { Controller, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import {
  Crud,
  CrudController,
  Override,
  ParsedBody,
  ParsedRequest,
} from '@nestjsx/crud';
import { GetUser } from '~/decorators';
import { ContentView } from './content-view.entity';
import { ContentViewsService } from './content-views.service';
import { CreateContentViewDto, UpdateContentViewDto } from './content-view.dto';
import { ContentOwnerGuard } from './guards';
import { ContentViewOwnerGuard } from './guards/content-view-owner.guard';

@Crud({
  model: { type: ContentView },
  dto: { create: CreateContentViewDto, update: UpdateContentViewDto },
  routes: {
    updateOneBase: { decorators: [UseGuards(ContentViewOwnerGuard)] },
    deleteOneBase: { decorators: [UseGuards(ContentViewOwnerGuard)] },
    replaceOneBase: { decorators: [UseGuards(ContentViewOwnerGuard)] },
  },
})
@ApiTags('content-views')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@Controller('content-views')
export class ContentViewsController implements CrudController<ContentView> {
  constructor(public service: ContentViewsService) {}

  @Override('createOneBase')
  @UseGuards(ContentOwnerGuard)
  createOne(
    @ParsedRequest() req,
    @ParsedBody() body: CreateContentViewDto,
    @GetUser('id') creatorId,
  ) {
    return this.service.createOne(req, { ...body, creatorId });
  }
}
