import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { User } from '~/users/user.entity';
import { ContentViewsService } from '../content-views.service';

@Injectable()
export class ContentViewOwnerGuard implements CanActivate {
  constructor(private readonly service: ContentViewsService) {}

  async canActivate(context: ExecutionContext) {
    const req = context.switchToHttp().getRequest();

    const user = req.user as User;
    const contentViewId = parseInt(req.params.id);

    const isUserCreator = await this.service.count({
      where: { id: contentViewId, creatorId: user.id },
    });

    if (!isUserCreator) {
      throw new ForbiddenException(
        'User does not have access for that content view',
      );
    }

    return true;
  }
}
