import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { ContentService } from '~/content/content.service';
import { User } from '~/users/user.entity';

@Injectable()
export class ContentOwnerGuard implements CanActivate {
  constructor(private readonly contentService: ContentService) {}

  async canActivate(context: ExecutionContext) {
    const req = context.switchToHttp().getRequest();

    const user = req.user as User;
    const contentId = parseInt(req.body.contentId);

    const isUserCreator = await this.contentService.count({
      where: { id: contentId, creatorId: user.id },
    });

    if (!isUserCreator) {
      throw new ForbiddenException(
        'User does not have access for that content',
      );
    }

    return true;
  }
}
