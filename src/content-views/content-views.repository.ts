import { EntityRepository, Repository } from 'typeorm';
import { ContentView } from './content-view.entity';

@EntityRepository(ContentView)
export class ContentViewRepository extends Repository<ContentView> {}
