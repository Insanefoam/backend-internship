import { Injectable } from '@nestjs/common';
import { InjectConnection } from '@nestjs/typeorm';
import { ContentView } from '~/content-views/content-view.entity';
import { StorageService } from '~/storage/storage.service';
import {
  Connection,
  EntitySubscriberInterface,
  EventSubscriber,
  RemoveEvent,
} from 'typeorm';

@Injectable()
@EventSubscriber()
export class RemoveContentViewSubscriber
  implements EntitySubscriberInterface<ContentView> {
  constructor(
    @InjectConnection() readonly connection: Connection,
    readonly storageService: StorageService,
  ) {
    connection.subscribers.push(this);
  }

  listenTo() {
    return ContentView;
  }

  async beforeRemove(event: RemoveEvent<ContentView>) {
    const fileLink = event.entity.link;
    if (fileLink) {
      const fileKey = fileLink.split('/').slice(-1)[0];
      await this.storageService.deleteObject(fileKey);
    }
  }
}
