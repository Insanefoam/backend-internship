import { OmitType, PartialType } from '@nestjs/swagger';
import { ContentView } from './content-view.entity';

export class CreateContentViewDto extends OmitType(ContentView, [
  'id',
  'content',
  'createdAt',
  'creator',
  'creatorId',
  'orientation',
  'updatedAt',
]) {}

export class UpdateContentViewDto extends PartialType(CreateContentViewDto) {}
