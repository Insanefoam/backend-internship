import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsUrl, Min } from 'class-validator';
import { Content } from '~/content/content.entity';
import { Orientation } from '~/orientations/orientation.entity';
import { User } from '~/users/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('content-views')
export class ContentView {
  @PrimaryGeneratedColumn('increment')
  @ApiProperty()
  id: number;

  @ManyToOne(() => Content, (content) => content.id, {
    onDelete: 'CASCADE',
    cascade: ['remove', 'update'],
  })
  @ApiProperty({ type: () => Content })
  @JoinColumn()
  content: Content;

  @Column()
  @ApiProperty()
  @IsNumber()
  contentId: number;

  @Column('varchar')
  @ApiProperty()
  @IsUrl()
  link: string;

  @Column()
  @ApiProperty()
  @Min(0, { message: 'Width must be positive value' })
  width: number;

  @Column()
  @ApiProperty()
  @Min(0, { message: 'Width must be positive value' })
  height: number;

  @ManyToOne(() => Orientation, (orientation) => orientation.id, {
    onDelete: 'SET NULL',
    nullable: true,
  })
  @JoinColumn()
  @ApiProperty({ type: () => Orientation })
  orientation: Orientation;

  @Column()
  @ApiProperty()
  orientationId: number;

  @ManyToOne(() => User, (user) => user.id)
  @JoinColumn()
  @ApiProperty({ type: () => User })
  creator: User;

  @ApiProperty()
  @Column()
  creatorId: number;

  @UpdateDateColumn({ type: 'timestamptz' })
  @ApiProperty()
  updatedAt: Date;

  @CreateDateColumn({ type: 'timestamptz' })
  @ApiProperty()
  createdAt: Date;
}
