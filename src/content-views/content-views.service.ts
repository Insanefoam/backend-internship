import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { ContentView } from './content-view.entity';
import { ContentViewRepository } from './content-views.repository';

@Injectable()
export class ContentViewsService extends TypeOrmCrudService<ContentView> {
  constructor(public repo: ContentViewRepository) {
    super(repo);
  }
}
