import { Module } from '@nestjs/common';
import { ContentViewsService } from './content-views.service';
import { ContentViewsController } from './content-views.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ContentModule } from '~/content/content.module';
import { RemoveContentViewSubscriber } from './subscribers';
import { StorageModule } from '~/storage/storage.module';
import { ContentViewRepository } from './content-views.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([ContentViewRepository]),
    ContentModule,
    StorageModule,
  ],
  controllers: [ContentViewsController],
  providers: [ContentViewsService, RemoveContentViewSubscriber],
})
export class ContentViewsModule {}
