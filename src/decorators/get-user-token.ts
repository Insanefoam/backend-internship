import { createParamDecorator } from '@nestjs/common';

export const GetUserToken = createParamDecorator((data, req): string => {
  const request = req.switchToHttp().getRequest();
  const token = (request.headers['authorization'] as string).replace(
    'Bearer ',
    '',
  );
  return token;
});
