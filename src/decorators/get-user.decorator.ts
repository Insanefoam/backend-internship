import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const GetUser = createParamDecorator(
  (field: string, context: ExecutionContext) => {
    const req = context.switchToHttp().getRequest();

    if (req.user) {
      return field ? req.user[field] : req.user;
    }

    return null;
  },
);
