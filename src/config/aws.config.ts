import { registerAs } from '@nestjs/config';

export const awsConfig = registerAs('awsConfig', () => {
  return {
    bucketName: process.env.S3_BUCKET_NAME,
    secretKey: process.env.S3_SECRET_KEY,
    accessKey: process.env.S3_ACCESS_KEY,
  };
});
