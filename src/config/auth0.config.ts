import { registerAs } from '@nestjs/config';

export const auth0Config = registerAs('auth0Config', () => {
  return {
    issuer: process.env.AUTH0_ISSUER_URL,
    domain: process.env.AUTH0_DOMAIN,
    audience: process.env.AUTH0_AUDIENCE,
  };
});
