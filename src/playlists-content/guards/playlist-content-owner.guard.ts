import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { getAction } from '@nestjsx/crud';
import { PlaylistsContentService } from '../playlists-content.service';

@Injectable()
export class PlaylistContentOwnerGuard implements CanActivate {
  constructor(
    private readonly playlistContentService: PlaylistsContentService,
  ) {}

  async canActivate(context: ExecutionContext) {
    const handler = context.getHandler();
    const action = getAction(handler);

    if (
      action === 'Read-All' ||
      action === 'Create-One' ||
      action === 'Read-One'
    )
      return true;

    const req = context.switchToHttp().getRequest();

    const user = req.user;
    const playlistContentId = +req.params.id;

    const isUserCreator = await this.playlistContentService.count({
      where: { id: playlistContentId, creatorId: user.id },
    });

    if (!isUserCreator) {
      throw new ForbiddenException(
        'User does not have access for that playlist content',
      );
    }

    return true;
  }
}
