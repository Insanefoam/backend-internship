import { Controller, Param, ParseIntPipe, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import {
  Crud,
  CrudController,
  CrudRequest,
  Override,
  ParsedBody,
  ParsedRequest,
} from '@nestjsx/crud';
import { GetUser } from '~/decorators';
import { PlaylistContentOwnerGuard } from './guards';
import { PlaylistContent } from './playlist-content.entity';
import {
  CreatePlaylistContentDto,
  UpdatePlaylistContentDto,
} from './playlists-content.dto';
import { PlaylistsContentService } from './playlists-content.service';

@Crud({
  model: { type: PlaylistContent },
  dto: { create: CreatePlaylistContentDto, update: UpdatePlaylistContentDto },
  query: { join: { content: {}, playlist: {} } },
})
@ApiTags('playlists-content')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'), PlaylistContentOwnerGuard)
@Controller('playlists-content')
export class PlaylistsContentController
  implements CrudController<PlaylistContent> {
  constructor(public service: PlaylistsContentService) {}

  @Override('createOneBase')
  createOne(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() body: CreatePlaylistContentDto,
    @GetUser('id') creatorId,
  ) {
    return this.service.createOne(req, { ...body, creatorId });
  }

  @Override('updateOneBase')
  updateOne(
    @ParsedRequest() req: CrudRequest,
    @Param('id', ParseIntPipe) id: number,
    @ParsedBody() body: UpdatePlaylistContentDto,
  ) {
    return this.service._updateOne(id, body);
  }
}
