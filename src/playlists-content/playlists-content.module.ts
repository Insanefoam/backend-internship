import { Module } from '@nestjs/common';
import { PlaylistsContentService } from './playlists-content.service';
import { PlaylistsContentController } from './playlists-content.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PlaylistsContentRepository } from './playlists-content.repository';

@Module({
  imports: [TypeOrmModule.forFeature([PlaylistsContentRepository])],
  providers: [PlaylistsContentService],
  controllers: [PlaylistsContentController],
  exports: [PlaylistsContentService],
})
export class PlaylistsContentModule {}
