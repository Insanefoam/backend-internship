import { OmitType, PartialType } from '@nestjs/swagger';
import { PlaylistContent } from './playlist-content.entity';

export class CreatePlaylistContentDto extends OmitType(PlaylistContent, [
  'id',
  'createdAt',
  'content',
  'playlist',
  'updatedAt',
  'creator',
  'creatorId',
]) {}

export class UpdatePlaylistContentDto extends PartialType(
  CreatePlaylistContentDto,
) {}
