import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, Min } from 'class-validator';
import { Content } from '~/content/content.entity';
import { Playlist } from '~/playlists/playlist.entity';
import { User } from '~/users/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('playlists-content')
export class PlaylistContent {
  @PrimaryGeneratedColumn('increment')
  @ApiProperty()
  id: number;

  @ManyToOne(() => Playlist, (playlist) => playlist.id)
  @ApiProperty({ type: () => Playlist })
  playlist: Playlist;

  @Column('int')
  @ApiProperty({ required: true })
  @IsNumber()
  playlistId: number;

  @ManyToOne(() => Content, (content) => content.id, {
    onDelete: 'CASCADE',
    cascade: ['remove'],
  })
  @ApiProperty({ type: () => Content })
  content: Content;

  @Column('int')
  @ApiProperty({ required: true })
  @IsNumber()
  contentId: number;

  @Column('int')
  @ApiProperty()
  @IsNumber({ allowNaN: false })
  @Min(1, { message: 'Order must be bigger or equal than 1' })
  order: number;

  @Column('int')
  @ApiProperty()
  @IsNumber({ allowNaN: false })
  @Min(0, { message: 'Duration cant be negative' })
  durationSeconds: number;

  @ManyToOne(() => User, (user) => user.id)
  @JoinColumn()
  @ApiProperty({ type: () => User })
  creator: User;

  @ApiProperty()
  @Column()
  creatorId: number;

  @UpdateDateColumn({ type: 'timestamptz' })
  @ApiProperty()
  updatedAt: Date;

  @CreateDateColumn({ type: 'timestamptz' })
  @ApiProperty()
  createdAt: Date;
}
