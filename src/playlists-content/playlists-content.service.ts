import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { PlaylistContent } from './playlist-content.entity';
import { UpdatePlaylistContentDto } from './playlists-content.dto';
import { PlaylistsContentRepository } from './playlists-content.repository';

@Injectable()
export class PlaylistsContentService extends TypeOrmCrudService<PlaylistContent> {
  constructor(public repo: PlaylistsContentRepository) {
    super(repo);
  }

  async _updateOne(id: number, dto: UpdatePlaylistContentDto) {
    const current = await this.repo.findOne(id);

    const isOrderChange = dto.order !== current.order;
    if (isOrderChange) {
      await this.changeOrderInPlaylist(
        current.playlistId,
        current.id,
        current.order,
        dto.order,
      );
    }

    return await this.repo.update(id, dto);
  }

  async changeOrderInPlaylist(
    playlistId: number,
    playlistContentId: number,
    prevOrder: number,
    newOrder: number,
  ) {
    const isMoveTop = prevOrder > newOrder;
    const isMoveBottom = prevOrder < newOrder;

    const playlistContent = await this.repo.find({
      where: { playlistId },
      order: { order: 'ASC' },
    });

    if (isMoveTop) {
      const contentToChange = playlistContent.slice(newOrder - 1);
      await Promise.all(
        contentToChange.map(async (content) => {
          await this.repo.update(content.id, { order: content.order + 1 });
        }),
      );
    }

    if (isMoveBottom) {
      const contentToChange = playlistContent.slice(0, newOrder);
      await Promise.all(
        contentToChange.map(async (content) => {
          await this.repo.update(content.id, { order: content.order - 1 });
        }),
      );
    }

    return await this.repo.update(playlistContentId, { order: newOrder });
  }
}
