import { EntityRepository, Repository } from 'typeorm';
import { PlaylistContent } from './playlist-content.entity';

@EntityRepository(PlaylistContent)
export class PlaylistsContentRepository extends Repository<PlaylistContent> {}
