import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { UsersController } from './users/users.controller';
import { EventsModule } from './events/events.module';
import { AuthModule } from './auth/auth.module';
import { ScreensModule } from './screens/screens.module';
import { PlaylistsModule } from './playlists/playlists.module';
import { ContentModule } from './content/content.module';
import { PlaylistsContentModule } from './playlists-content/playlists-content.module';
import { ContentViewsModule } from './content-views/content-views.module';
import { StorageModule } from './storage/storage.module';
import { LoggerModule } from 'nestjs-pino';
import { OrientationsModule } from './orientations/orientations.module';
import { ConfigModule } from '@nestjs/config';
import { auth0Config, awsConfig } from './config';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true, load: [awsConfig, auth0Config] }),
    TypeOrmModule.forRoot(),
    LoggerModule.forRoot({
      pinoHttp: {
        prettyPrint: { colorize: true },
        level: 'error',
      },
    }),
    UsersModule,
    EventsModule,
    AuthModule,
    ScreensModule,
    PlaylistsModule,
    ContentModule,
    PlaylistsContentModule,
    ContentViewsModule,
    StorageModule,
    OrientationsModule,
  ],
  controllers: [AppController, UsersController],
  providers: [AppService],
})
export class AppModule {}
