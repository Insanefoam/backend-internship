import { ApiProperty } from '@nestjs/swagger';
import { User } from '~/users/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('playlists')
export class Playlist {
  @PrimaryGeneratedColumn('increment')
  @ApiProperty()
  id: number;

  @ManyToOne(() => User, (user) => user.id)
  @JoinColumn()
  @ApiProperty({ type: () => User })
  creator: User;

  @ApiProperty()
  @Column()
  creatorId: number;

  @CreateDateColumn({ type: 'timestamptz' })
  @ApiProperty()
  createdAt: Date;
}
