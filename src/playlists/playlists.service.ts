import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { PlaylistsContentService } from '~/playlists-content/playlists-content.service';
import { Playlist } from './playlist.entity';
import { PlaylistsRepository } from './playlists.repository';

@Injectable()
export class PlaylistsService extends TypeOrmCrudService<Playlist> {
  constructor(
    public repo: PlaylistsRepository,
    private readonly playlistsContentService: PlaylistsContentService,
  ) {
    super(repo);
  }

  async getPlaylistsContent(playlistId: number) {
    return await this.playlistsContentService.find({
      where: { playlistId: playlistId },
      relations: ['content'],
    });
  }
}
