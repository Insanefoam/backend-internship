import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { PlaylistsService } from '../playlists.service';

@Injectable()
export class PlaylistOwnerGuard implements CanActivate {
  constructor(private readonly playlistsService: PlaylistsService) {}

  async canActivate(context: ExecutionContext) {
    const req = context.switchToHttp().getRequest();

    const user = req.user;
    const playlistId = +req.params.id;

    const isUserCreator = await this.playlistsService.count({
      where: { id: playlistId, creatorId: user.id },
    });

    if (!isUserCreator) {
      throw new ForbiddenException(
        'User does not have access for that playlist',
      );
    }

    return true;
  }
}
