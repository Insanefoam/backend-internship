import { Module } from '@nestjs/common';
import { PlaylistsService } from './playlists.service';
import { PlaylistsController } from './playlists.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PlaylistsContentModule } from '~/playlists-content/playlists-content.module';
import { PlaylistsRepository } from './playlists.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([PlaylistsRepository]),
    PlaylistsContentModule,
  ],
  providers: [PlaylistsService],
  controllers: [PlaylistsController],
  exports: [PlaylistsService],
})
export class PlaylistsModule {}
