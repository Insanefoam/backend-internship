import {
  Controller,
  Get,
  Param,
  ParseIntPipe,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { PlaylistContent } from '~/playlists-content/playlist-content.entity';
import { PlaylistOwnerGuard } from './guards';
import { Playlist } from './playlist.entity';
import { PlaylistsService } from './playlists.service';

@Crud({
  model: { type: Playlist },
  routes: {
    exclude: [
      'createManyBase',
      'createOneBase',
      'updateOneBase',
      'replaceOneBase',
    ],
    deleteOneBase: {
      decorators: [UseGuards(PlaylistOwnerGuard)],
    },
  },
})
@ApiTags('playlists')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@Controller('playlists')
export class PlaylistsController implements CrudController<Playlist> {
  constructor(public service: PlaylistsService) {}

  @Get(':id/playlists-content')
  @ApiOkResponse({ type: () => PlaylistContent, isArray: true })
  async getPlaylistsContent(@Param('id', ParseIntPipe) id: number) {
    return this.service.getPlaylistsContent(id);
  }
}
