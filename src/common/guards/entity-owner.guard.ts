import { CanActivate, ExecutionContext } from '@nestjs/common';

interface Ownable {
  authorId?: any;
}

interface EntityOwnerGuardService {
  getSingleEntity: (id: any) => Promise<Ownable>;
}

export class EntityOwnerGuard implements CanActivate {
  constructor(public entityService: EntityOwnerGuardService) {}

  async canActivate(context: ExecutionContext) {
    const req = context.switchToHttp().getRequest();
    const userId = req.user.id;

    const entityId = req.params.id;
    const entity = await this.entityService.getSingleEntity(entityId);

    if (entity.authorId !== userId) {
      return false;
    }

    return true;
  }
}
