import { Controller, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import {
  Crud,
  CrudController,
  CrudRequest,
  Override,
  ParsedBody,
  ParsedRequest,
} from '@nestjsx/crud';
import { GetUser } from '~/decorators';
import { OrientationOwnerGuard } from './guards';
import { Orientation } from './orientation.entity';
import {
  CreateOrientationBodyDto,
  UpdateOrientationBodyDto,
} from './orientations.dto';
import { OrientationsService } from './orientations.service';

@Crud({
  model: { type: Orientation },
  dto: { create: CreateOrientationBodyDto, update: UpdateOrientationBodyDto },
  routes: { updateOneBase: { decorators: [UseGuards(OrientationOwnerGuard)] } },
})
@ApiTags('orientations')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@Controller('orientations')
export class OrientationsController implements CrudController<Orientation> {
  constructor(public service: OrientationsService) {}

  @Override('createOneBase')
  createOne(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() body: CreateOrientationBodyDto,
    @GetUser('id') userId: number,
  ) {
    return this.service.createOne(req, { ...body, authorId: userId });
  }
}
