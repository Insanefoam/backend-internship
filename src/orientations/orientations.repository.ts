import { Injectable } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { Orientation } from './orientation.entity';

@EntityRepository(Orientation)
@Injectable()
export class OrientationsRepository extends Repository<Orientation> {}
