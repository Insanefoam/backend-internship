import { ApiProperty } from '@nestjs/swagger';
import { User } from '~/users/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'orientations' })
export class Orientation {
  @PrimaryGeneratedColumn('increment')
  @ApiProperty()
  id: number;

  @Column({ type: 'varchar' })
  @ApiProperty()
  name: string;

  @Column({ type: 'numeric', default: 16 })
  @ApiProperty()
  widthCoefficient: number;

  @Column({ type: 'numeric', default: 9 })
  @ApiProperty()
  heightCoefficient: number;

  @ManyToOne(() => User, (user) => user.id, { nullable: true })
  @JoinColumn()
  @ApiProperty({ type: () => User })
  author?: User;

  @Column({ nullable: true })
  @ApiProperty({ required: false })
  authorId?: number;

  @CreateDateColumn({ type: 'timestamptz' })
  createdAt: string;
}
