import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Orientation } from './orientation.entity';
import { OrientationsRepository } from './orientations.repository';

@Injectable()
export class OrientationsService extends TypeOrmCrudService<Orientation> {
  constructor(repo: OrientationsRepository) {
    super(repo);
  }

  async getSingleEntity(id: number) {
    return this.repo.findOne(id);
  }
}
