import { Injectable } from '@nestjs/common';
import { EntityOwnerGuard } from '~/common/guards';
import { OrientationsService } from '../orientations.service';

@Injectable()
export class OrientationOwnerGuard extends EntityOwnerGuard {
  constructor(public entityService: OrientationsService) {
    super(entityService);
  }
}
