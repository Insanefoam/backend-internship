import { Module } from '@nestjs/common';
import { OrientationsService } from './orientations.service';
import { OrientationsController } from './orientations.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrientationsRepository } from './orientations.repository';

@Module({
  imports: [TypeOrmModule.forFeature([OrientationsRepository])],
  controllers: [OrientationsController],
  providers: [OrientationsService],
})
export class OrientationsModule {}
