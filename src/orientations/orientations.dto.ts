import { OmitType, PartialType } from '@nestjs/swagger';
import { Orientation } from './orientation.entity';

export class CreateOrientationDto extends OmitType(Orientation, [
  'author',
  'createdAt',
  'id',
]) {}

export class CreateOrientationBodyDto extends OmitType(CreateOrientationDto, [
  'authorId',
]) {}

export class UpdateOrientationBodyDto extends PartialType(
  CreateOrientationBodyDto,
) {}
