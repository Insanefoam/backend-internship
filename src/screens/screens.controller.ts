import {
  Controller,
  Get,
  Param,
  ParseIntPipe,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import {
  Crud,
  CrudAuth,
  CrudController,
  Override,
  ParsedBody,
} from '@nestjsx/crud';
import { GetUser } from '~/decorators';
import { Playlist } from '~/playlists/playlist.entity';
import { EventOwnerGuard, ScreenOwnerGuard } from './guards';
import { CreateScreenDto, UpdateScreenDto } from './screen.dto';
import { Screen } from './screen.entity';
import { ScreensService } from './screens.service';

@Crud({
  model: { type: Screen },
  dto: { create: CreateScreenDto, update: UpdateScreenDto },
  routes: {
    exclude: ['createManyBase'],
  },
  query: {
    join: {
      event: {
        eager: true,
      },
      playlist: {},
    },
  },
})
@CrudAuth({ filter: (req) => ({ creatorId: req.user.id }) })
@ApiBearerAuth()
@ApiTags('screens')
@UseGuards(AuthGuard('jwt'), ScreenOwnerGuard)
@Controller('screens')
export class ScreensController implements CrudController<Screen> {
  constructor(public service: ScreensService) {}

  @Override('createOneBase')
  @ApiOperation({ summary: 'Create one' })
  @UseGuards(EventOwnerGuard)
  async createOne(
    @ParsedBody() body: CreateScreenDto,
    @GetUser('id') creatorId,
  ) {
    return this.service.create(body, creatorId);
  }

  @Get(':id/playlist')
  @ApiOkResponse({ type: Playlist })
  async getScreenPlaylist(@Param('id', ParseIntPipe) id: number) {
    return this.service.getScreenPlaylists(id);
  }
}
