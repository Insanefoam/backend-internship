import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { getAction } from '@nestjsx/crud';
import { User } from '~/users/user.entity';
import { ScreensService } from '../screens.service';

@Injectable()
export class ScreenOwnerGuard implements CanActivate {
  constructor(private readonly screensService: ScreensService) {}

  async canActivate(context: ExecutionContext) {
    const handler = context.getHandler();
    const action = getAction(handler);

    if (
      action === 'Read-All' ||
      action === 'Create-One' ||
      action === 'Read-One'
    )
      return true;

    const req = context.switchToHttp().getRequest();

    const user = req.user as User;
    const screenId = +req.params.id;

    const isUserCreator = await this.screensService.count({
      where: { id: screenId, creatorId: user.id },
    });

    if (!isUserCreator) {
      throw new ForbiddenException('User does not have access for that screen');
    }

    return true;
  }
}
