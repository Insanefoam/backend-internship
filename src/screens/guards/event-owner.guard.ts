import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { EventsService } from '~/events/events.service';
import { User } from '~/users/user.entity';

@Injectable()
export class EventOwnerGuard implements CanActivate {
  constructor(private readonly eventsService: EventsService) {}

  async canActivate(context: ExecutionContext) {
    const req = context.switchToHttp().getRequest();

    const user = req.user as User;
    const eventId = +req.body.eventId;

    const isUserCreator = await this.eventsService.count({
      where: { id: eventId, creatorId: user.id },
    });

    if (!isUserCreator) {
      throw new ForbiddenException('User does not have access for that event');
    }

    return true;
  }
}
