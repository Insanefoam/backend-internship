import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Playlist } from '~/playlists/playlist.entity';
import { CreateScreenDto } from './screen.dto';
import { Screen } from './screen.entity';
import { ScreensRepository } from './screens.repository';
import { PlaylistsRepository } from '~/playlists/playlists.repository';

@Injectable()
export class ScreensService extends TypeOrmCrudService<Screen> {
  constructor(
    public repo: ScreensRepository,
    private readonly playlistsRepository: PlaylistsRepository,
  ) {
    super(repo);
  }

  async create(dto: CreateScreenDto, creatorId: number) {
    const playlist = await this.playlistsRepository.save({ creatorId });

    const screen = this.repo.create({ ...dto, creatorId, playlist });

    return await this.repo.save(screen);
  }

  async getScreenPlaylists(screenId: number): Promise<Playlist> {
    const screen = await this.repo.findOne(screenId);

    const playlist = await this.playlistsRepository.findOne({
      where: { id: screen.playlistId },
    });

    return playlist;
  }
}
