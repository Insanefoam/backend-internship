import { OmitType, PartialType } from '@nestjs/swagger';
import { Screen } from './screen.entity';

export class CreateScreenDto extends OmitType(Screen, [
  'id',
  'orientation',
  'playlist',
  'playlistId',
  'event',
  'createdAt',
  'creator',
  'creatorId',
]) {}

export class UpdateScreenDto extends PartialType(CreateScreenDto) {}
