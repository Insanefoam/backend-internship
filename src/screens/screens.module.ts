import { forwardRef, Module } from '@nestjs/common';
import { ScreensService } from './screens.service';
import { ScreensController } from './screens.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventsModule } from '~/events/events.module';
import { ContentModule } from '~/content/content.module';
import { ScreensRepository } from './screens.repository';
import { PlaylistsRepository } from '~/playlists/playlists.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([ScreensRepository, PlaylistsRepository]),
    forwardRef(() => EventsModule),
    ContentModule,
  ],
  providers: [ScreensService],
  controllers: [ScreensController],
  exports: [ScreensService],
})
export class ScreensModule {}
