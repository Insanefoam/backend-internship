import { ApiProperty } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';
import { Event } from '~/events/event.entity';
import { Orientation } from '~/orientations/orientation.entity';
import { Playlist } from '~/playlists/playlist.entity';
import { User } from '~/users/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('screens')
export class Screen {
  @PrimaryGeneratedColumn('increment')
  @ApiProperty()
  id: number;

  @ManyToOne(() => Event, (event) => event.id)
  @ApiProperty({ type: () => Event })
  event: Event;

  @Column('int')
  @ApiProperty({ nullable: false })
  @IsNumber()
  eventId: number;

  @OneToOne(() => Playlist, (playlist) => playlist.id)
  @JoinColumn()
  @ApiProperty({ type: () => Playlist })
  playlist: Playlist;

  @Column('int')
  @ApiProperty({ nullable: false })
  playlistId: number;

  @ManyToOne(() => User, (user) => user.id, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  @ApiProperty({ type: () => User })
  creator: User;

  @ManyToOne(() => Orientation, (orientation) => orientation.id, {
    onDelete: 'SET NULL',
    nullable: true,
  })
  @JoinColumn()
  @ApiProperty({ type: () => Orientation })
  orientation: Orientation;

  @Column()
  @ApiProperty()
  orientationId: number;

  @Column()
  @ApiProperty()
  creatorId: number;

  @CreateDateColumn({ type: 'timestamptz' })
  @ApiProperty()
  createdAt: Date;
}
