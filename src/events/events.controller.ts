import {
  Controller,
  Get,
  Param,
  ParseIntPipe,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import {
  Crud,
  CrudController,
  CrudRequest,
  Override,
  ParsedBody,
  ParsedRequest,
} from '@nestjsx/crud';
import { Event } from './event.entity';
import { EventsService } from './events.service';
import { CreateEventDto, UpdateEventDto } from './events.dto';
import { ScreensService } from '~/screens/screens.service';
import { Screen } from '~/screens/screen.entity';
import { EventOwnerGuard } from './guards';
import { GetUser } from '~/decorators';
import { AuthGuard } from '@nestjs/passport';

@Crud({
  model: { type: Event },
  dto: { create: CreateEventDto, update: UpdateEventDto },
  query: {
    join: {
      creator: {},
    },
  },
})
@ApiTags('events')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'), EventOwnerGuard)
@Controller('events')
export class EventsController implements CrudController<Event> {
  constructor(
    public service: EventsService,
    public screensService: ScreensService,
  ) {}

  @ApiOperation({ summary: 'Get event screens' })
  @ApiOkResponse({ type: () => Screen, isArray: true })
  @Get(':id/screens')
  async getEventScreens(@Param('id', ParseIntPipe) eventId: number) {
    return this.screensService.find({ where: { eventId } });
  }

  @Override('createOneBase')
  async create(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() body: CreateEventDto,
    @GetUser('id') creatorId,
  ) {
    return this.service.createOne(req, { ...body, creatorId });
  }
}
