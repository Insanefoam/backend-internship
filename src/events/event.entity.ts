import { ApiProperty } from '@nestjs/swagger';
import { IsString, Length } from 'class-validator';
import { Screen } from '~/screens/screen.entity';
import { User } from '~/users/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('events')
export class Event {
  @PrimaryGeneratedColumn('increment')
  @ApiProperty()
  id: number;

  @Column('varchar', { length: 200 })
  @ApiProperty()
  @Length(1, 200, {
    message: 'Event name must be bigger than 1 symbol and less than 200',
  })
  @IsString()
  name: string;

  @ManyToOne(() => User, (user) => user.id, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  @ApiProperty({ type: () => User })
  creator: User;

  @ApiProperty()
  @Column()
  creatorId: number;

  @OneToMany(() => Screen, (screen) => screen.id)
  @ApiProperty()
  screens: Screen[];

  @CreateDateColumn({ type: 'timestamptz' })
  @ApiProperty()
  createdAt: Date;
}
