import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { getAction } from '@nestjsx/crud';
import { User } from '~/users/user.entity';
import { EventsService } from '../events.service';

@Injectable()
export class EventOwnerGuard implements CanActivate {
  constructor(private readonly eventsService: EventsService) {}

  async canActivate(context: ExecutionContext) {
    const handler = context.getHandler();
    const action = getAction(handler);

    if (
      action === 'Read-All' ||
      action === 'Create-One' ||
      action === 'Read-One'
    )
      return true;

    const req = context.switchToHttp().getRequest();

    const user = req.user as User;
    const eventId = +req.params.id;

    const isUserCreator = await this.eventsService.count({
      where: { id: eventId, creatorId: user.id },
    });

    if (!isUserCreator) {
      throw new ForbiddenException('User does not have access for that event');
    }

    return true;
  }
}
