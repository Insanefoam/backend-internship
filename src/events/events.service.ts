import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { EventsRepository } from './events.repository';
import { Event } from './event.entity';

@Injectable()
export class EventsService extends TypeOrmCrudService<Event> {
  constructor(public repo: EventsRepository) {
    super(repo);
  }
}
