import { OmitType } from '@nestjs/swagger';
import { Event } from './event.entity';

export class CreateEventDto extends OmitType(Event, [
  'id',
  'createdAt',
  'creator',
  'creatorId',
  'screens',
]) {}

export class UpdateEventDto extends CreateEventDto {}
