import { EntityRepository, Repository } from 'typeorm';
import { User } from './user.entity';

@EntityRepository(User)
export class UsersRepository extends Repository<User> {
  async findOrCreate(dto: Partial<User>) {
    const { id } = dto;

    const existed = await this.findOne(id);

    if (existed) {
      return existed;
    }

    return await this.save(dto);
  }
}
