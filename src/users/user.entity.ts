import { IsEmail, Length } from 'class-validator';
import { Event } from '~/events/event.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';

@Entity({ name: 'users' })
export class User {
  @ApiProperty()
  @PrimaryGeneratedColumn('increment')
  id: number;

  @ApiProperty()
  @Column({ type: 'varchar', default: '1' })
  auth0Id: string;

  @Column({ unique: true })
  @IsEmail()
  @ApiProperty()
  email: string;

  @Exclude({ toPlainOnly: true })
  @Column({ type: 'varchar', length: 150, nullable: true })
  @ApiProperty()
  password?: string;

  @Column({ type: 'varchar', length: 50, nullable: true })
  @Length(1, 50)
  @ApiProperty()
  firstName?: string;

  @Column({ type: 'varchar', length: 50, nullable: true })
  @Length(1, 50)
  @ApiProperty()
  lastName?: string;

  @OneToMany(() => Event, (event) => event.creator)
  @ApiProperty({ type: () => Event, isArray: true })
  events: Event[];

  @CreateDateColumn({ type: 'timestamptz' })
  @ApiProperty()
  createdAt: Date;
}
