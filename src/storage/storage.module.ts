import { Module } from '@nestjs/common';
import { StorageService } from './storage.service';
import { StorageController } from './storage.controller';
import { s3Factory } from './storage.factory';

@Module({
  controllers: [StorageController],
  providers: [StorageService, s3Factory],
  exports: [StorageService, s3Factory],
})
export class StorageModule {}
