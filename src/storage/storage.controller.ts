import {
  Body,
  Controller,
  Delete,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { PresignUrlDto } from './storage.dto';
import { StorageService } from './storage.service';

@ApiTags('storage')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@Controller('storage')
export class StorageController {
  constructor(private readonly storageService: StorageService) {}

  @Post('presign-url')
  @ApiOkResponse({ type: String })
  async getPresignUrl(@Body() body: PresignUrlDto) {
    return this.storageService.presignUrl(body);
  }

  @Delete(':key')
  @ApiOkResponse({ type: Boolean })
  async deleteFile(@Param('key') key: string) {
    return this.storageService.deleteObject(key);
  }
}
