import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsIn } from 'class-validator';

const validContentTypes = [
  'image/jpeg',
  'image/png',
  'image/gif',
  'video/mp4',
  'video/x-msvideo', // .avi
  'video/quicktime', // .mov
  'audio/mp4',
  'text/html',
];

export class PresignUrlDto {
  @ApiProperty()
  @IsDefined()
  key: string;

  @ApiProperty()
  @IsDefined()
  @IsIn(validContentTypes)
  contentType: string;
}
