import { S3 } from 'aws-sdk';
import { Provider } from '@nestjs/common';
import { awsConfig } from '~/config';
import { ConfigType } from '@nestjs/config';

export const s3Factory: Provider = {
  provide: 'S3_STORAGE',
  useFactory: (config: ConfigType<typeof awsConfig>) => {
    return new S3({
      credentials: {
        accessKeyId: config.accessKey,
        secretAccessKey: config.secretKey,
      },
      region: 'eu-north-1',
      signatureVersion: 'v4',
    });
  },
  inject: [awsConfig.KEY],
};
