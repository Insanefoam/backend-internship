import { Inject, Injectable } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { S3 } from 'aws-sdk';
import { awsConfig } from '~/config';
import { PresignUrlDto } from './storage.dto';

@Injectable()
export class StorageService {
  constructor(
    @Inject('S3_STORAGE') private readonly s3Client: S3,
    @Inject(awsConfig.KEY) private config: ConfigType<typeof awsConfig>,
  ) {}

  private expiresDuration = 10 * 60;

  async presignUrl(body: PresignUrlDto) {
    return await this.s3Client.getSignedUrlPromise('putObject', {
      Bucket: this.config.bucketName,
      Key: body.key,
      Expires: this.expiresDuration,
      ContentType: body.contentType,
    });
  }

  async deleteObject(key: string) {
    console.log('delete object with key', key);
    return await new Promise((resolve, reject) =>
      this.s3Client.deleteObject(
        {
          Bucket: this.config.bucketName,
          Key: key,
        },
        (err, data) => {
          if (!err) {
            resolve(true);
          }
          reject(err);
        },
      ),
    );
  }
}
