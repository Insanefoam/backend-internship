import { Controller, Get, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { AuthService } from './auth/auth.service';
import { GetUser } from './decorators';
import { GetUserToken } from './decorators/get-user-token';
import { User } from './users/user.entity';

@Controller()
export class AppController {
  constructor(private readonly authService: AuthService) {}

  @ApiOperation({ summary: 'Get me' })
  @ApiResponse({ type: User })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Get('/auth/me')
  async getMe(@GetUser() user: User): Promise<User> {
    return user;
  }

  @ApiOperation({ summary: 'Login' })
  @ApiResponse({ type: User })
  @ApiBearerAuth()
  @Post('auth/login')
  async login(@GetUserToken() token: string): Promise<User> {
    return this.authService.loginUser(token);
  }
}
