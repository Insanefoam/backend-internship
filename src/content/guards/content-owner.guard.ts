import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { getAction } from '@nestjsx/crud';
import { User } from '~/users/user.entity';
import { ContentService } from '../content.service';

@Injectable()
export class ContentOwnerGuard implements CanActivate {
  constructor(private readonly contentService: ContentService) {}

  async canActivate(context: ExecutionContext) {
    const handler = context.getHandler();
    const action = getAction(handler);

    if (
      action === 'Read-All' ||
      action === 'Read-One' ||
      action === 'Create-One'
    )
      return true;

    const req = context.switchToHttp().getRequest();

    const user = req.user as User;
    const contentId = +req.params.id;

    const isUserCreator = await this.contentService.count({
      where: { id: contentId, creatorId: user.id },
    });

    if (!isUserCreator) {
      throw new ForbiddenException(
        'User does not have access for that content',
      );
    }

    return true;
  }
}
