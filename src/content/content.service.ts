import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Content } from './content.entity';
import { ContentRepository } from './content.repository';

@Injectable()
export class ContentService extends TypeOrmCrudService<Content> {
  constructor(public repo: ContentRepository) {
    super(repo);
  }
}
