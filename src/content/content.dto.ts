import { OmitType, PartialType } from '@nestjs/swagger';
import { Content } from './content.entity';

export class CreateContentDto extends OmitType(Content, [
  'id',
  'contentViews',
  'creator',
  'creatorId',
  'createdAt',
]) {}

export class UpdateContentDto extends PartialType(CreateContentDto) {}
