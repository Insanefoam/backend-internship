import { Controller, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import {
  Crud,
  CrudController,
  CrudRequest,
  Override,
  ParsedBody,
  ParsedRequest,
} from '@nestjsx/crud';
import { GetUser } from '~/decorators';
import { CreateContentDto, UpdateContentDto } from './content.dto';
import { Content } from './content.entity';
import { ContentService } from './content.service';
import { ContentOwnerGuard } from './guards';

@Crud({
  model: { type: Content },
  dto: {
    create: CreateContentDto,
    update: UpdateContentDto,
  },
})
@ApiTags('content')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'), ContentOwnerGuard)
@Controller('content')
export class ContentController implements CrudController<Content> {
  constructor(public service: ContentService) {}

  @Override('createOneBase')
  createOne(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() body: CreateContentDto,
    @GetUser('id') creatorId: number,
  ) {
    return this.service.createOne(req, { ...body, creatorId });
  }
}
