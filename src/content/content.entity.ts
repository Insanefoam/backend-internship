import { ApiProperty } from '@nestjs/swagger';
import { IsEnum } from 'class-validator';
import { ContentView } from '~/content-views/content-view.entity';
import { User } from '~/users/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

type ContentType = 'video' | 'audio' | 'image' | 'html';
const contentTypeEnum: ContentType[] = ['video', 'audio', 'image', 'html'];

@Entity()
export class Content {
  @PrimaryGeneratedColumn('increment')
  @ApiProperty()
  id: number;

  @Column({ type: 'enum', enum: contentTypeEnum })
  @ApiProperty({ type: 'enum', enum: contentTypeEnum })
  @IsEnum(contentTypeEnum)
  type: ContentType;

  @OneToMany(() => ContentView, (contentView) => contentView.contentId, {
    onDelete: 'CASCADE',
    cascade: ['remove', 'update'],
  })
  @ApiProperty({ type: () => ContentView, isArray: true })
  contentViews: ContentView[];

  @ManyToOne(() => User, (user) => user.id)
  @JoinColumn()
  @ApiProperty({ type: () => User })
  creator: User;

  @ApiProperty()
  @Column()
  creatorId: number;

  @CreateDateColumn({ type: 'timestamptz' })
  @ApiProperty()
  createdAt: Date;
}
